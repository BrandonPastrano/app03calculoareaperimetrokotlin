package com.example.app03calculoareaperimetrokotlin

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity

class Rectangulo_Activity : AppCompatActivity() {
    private lateinit var rectangulo: Rectangulo
    private lateinit var txtBase: EditText
    private lateinit var txtAltura: EditText
    private lateinit var txtCalArea: TextView
    private lateinit var txtCalPerimetro: TextView
    private lateinit var txtNombree: TextView
    private lateinit var btnCalcular: Button
    private lateinit var btnLimpiar: Button
    private lateinit var btnRegresar: Button

    private fun iniciar() {
        txtNombree = findViewById(R.id.etxtNombre)
        txtBase = findViewById(R.id.etxtBase)
        txtAltura = findViewById(R.id.etxtAltura)
        txtCalArea = findViewById(R.id.etxtArea)
        txtCalPerimetro = findViewById(R.id.etxtPerimetro)
        btnCalcular = findViewById(R.id.btnCalcular)
        btnRegresar = findViewById(R.id.btnRegresar)
        btnLimpiar = findViewById(R.id.btnLimpiar)
    }
    private fun validar(): Boolean {
        var exito = true
        if (txtAltura.text.toString() == "") exito = false
        if (txtBase.text.toString() == "") exito = false
        return exito
    }
    private fun btnCalcular() {
        rectangulo = Rectangulo(txtBase.text.toString().toInt(), txtAltura.text.toString().toInt())
        val area = rectangulo.calcularCalcularArea()
        val perimetro = rectangulo.calcularPagPerimetro()
        txtCalArea.text = " $area"
        txtCalPerimetro.text = " $perimetro"
    }
    private fun btnLimpiar() {
        txtAltura.setText("")
        txtBase.setText("")
        txtCalArea.text = ""
        txtCalPerimetro.text = ""
        rectangulo = Rectangulo(0, 0)
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_rectangulo)
        iniciar()
        val nombre = intent.getStringExtra("nombre")
        txtNombree.text = nombre
        btnRegresar.setOnClickListener{
            val intent = Intent(this@Rectangulo_Activity, MainActivity::class.java)
            startActivity(intent)
            finish()
        }

        btnCalcular.setOnClickListener(View.OnClickListener {
            if (validar()) {
                btnCalcular()
            } else {
                Toast.makeText(applicationContext, "Falto capturar datos", Toast.LENGTH_SHORT)
                    .show()
            }
        })
        btnLimpiar.setOnClickListener(View.OnClickListener { btnLimpiar(); })
    }
}