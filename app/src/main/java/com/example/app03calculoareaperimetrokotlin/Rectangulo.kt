package com.example.app03calculoareaperimetrokotlin

class Rectangulo {
    var base: Int = 0;
    var altura: Int = 0;

    //constructores Secundarios
    constructor(base: Int, altura: Int) {
        this.base = base
        this.altura = altura
    }
    //metodos
    open fun calcularCalcularArea(): Float {
        var area = altura.toFloat();
        area = area * base;
        return area;
    }

    fun calcularPagPerimetro(): Float {
        var perimetro = base.toFloat();
        perimetro = (2 * (base + altura).toFloat());
        return perimetro;
    }
}