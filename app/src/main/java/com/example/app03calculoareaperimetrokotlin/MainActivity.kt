package com.example.app03calculoareaperimetrokotlin

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.EditText
import androidx.appcompat.app.AlertDialog

class MainActivity : AppCompatActivity() {
    private lateinit var txtNombre:EditText
    private lateinit var btnEntrar:Button
    private lateinit var btnSalir:Button

    private fun btnSalir() {
        val confirmar = AlertDialog.Builder(this)
        confirmar.setTitle("¿Cerrar APP?")
        confirmar.setMessage("Se descartara toda la información ingresada.")
        confirmar.setPositiveButton("Confirmar") { dialogInterface, which -> aceptar() }
        confirmar.setNegativeButton("Cancelar") { dialogInterface, i ->
            //Nada
        }
        confirmar.show()
    }

    private fun aceptar() {
        finish()
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        txtNombre = findViewById(R.id.txtNombre)
        btnEntrar = findViewById(R.id.btnEntrar)
        btnSalir = findViewById(R.id.btnSalir)
        btnEntrar.setOnClickListener{
            val intent = Intent(this@MainActivity, Rectangulo_Activity::class.java)
            intent.putExtra("nombre", txtNombre!!.text.toString())
            startActivity(intent)
            finish()
        }
        btnSalir.setOnClickListener(View.OnClickListener{ btnSalir() })
    }
}